Pod::Spec.new do |s|
  s.name = 'TYShowMessageHandler'
  s.version = '0.0.3'
  s.platform = :ios, '8.0'
  s.license = { type: 'MIT', file: 'LICENSE' }
  s.summary = 'TYShowMessageHandler'
  s.homepage = 'https://gitlab.com/xwgit2971/TYShowMessageHandler'
  s.author = { 'SunnyX' => '1031787148@qq.com' }
  s.source = { :git => 'git@gitlab.com:xwgit2971/TYShowMessageHandler.git', :tag => s.version }
  s.source_files = 'TYShowMessageHandler/*.{h,m}'
  s.framework = 'Foundation', 'UIKit'
  s.requires_arc = true
  s.dependency  'MBProgressHUD', '~> 1.0.0'
  s.dependency  'JDStatusBarNotification', '~> 1.5.5'
end
