//
//  TYShowMessageHandler.m
//  TYShowMessageHandler
//
//  Created by 夏伟 on 2016/11/3.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "TYShowMessageHandler.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <JDStatusBarNotification/JDStatusBarNotification.h>

#define kHUDQueryViewTag 1010

@implementation TYShowMessageHandler

#pragma mark - MBProgressHUD
+ (void)ty_showHUDQueryStr:(NSString *)titleStr {
    [self ty_showHUDQueryStr:titleStr toView:nil];
}

+ (void)ty_showHUDQueryStr:(NSString *)titleStr toView:(UIView *)view {
    titleStr = titleStr.length > 0 ? titleStr : @"正在获取数据...";
    if (view == nil) view = [UIApplication sharedApplication].keyWindow;
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.tag = kHUDQueryViewTag;
    hud.label.text = titleStr;
    hud.label.font = [UIFont boldSystemFontOfSize:15.0];
    hud.margin = 10.f;
}

+ (NSUInteger)ty_hideHUDQuery {
    return [self ty_hideHUDQueryForView:nil];
}

+ (NSUInteger)ty_hideHUDQueryForView:(UIView *)view {
    if (view == nil) view = [UIApplication sharedApplication].keyWindow;
    __block NSUInteger count = 0;
    NSArray *huds = [MBProgressHUD allHUDsForView:view];
    [huds enumerateObjectsUsingBlock:^(UIView *obj, NSUInteger idx, BOOL *stop) {
        if (obj.tag == kHUDQueryViewTag) {
            [obj removeFromSuperview];
            count++;
        }
    }];
    return count;
}

+ (void)ty_show:(NSString *)text icon:(NSString *)icon {
    [self ty_show:text icon:icon view:nil];
}

+ (void)ty_show:(NSString *)text icon:(NSString *)icon view:(UIView *)view {
    if (view == nil) view = [UIApplication sharedApplication].keyWindow;
    // 快速显示一个提示信息
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.label.text = text;
    // 设置图片
    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:icon]];
    // 再设置模式
    hud.mode = MBProgressHUDModeCustomView;
    // 隐藏时候从父控件中移除
    hud.removeFromSuperViewOnHide = YES;
    // 1秒之后再消失
    [hud hideAnimated:YES afterDelay:0.7];
}

+ (void)ty_showHudTipStr:(NSString *)tipStr {
    if (tipStr && tipStr.length > 0) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.detailsLabel.font = [UIFont boldSystemFontOfSize:15.0];
        hud.detailsLabel.text = tipStr;
        hud.margin = 10.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hideAnimated:YES afterDelay:1.0];
    }
}

#pragma mark - JDStatusBarNotification
+ (void)ty_showStatusBarQueryStr:(NSString *)tipStr {
    [JDStatusBarNotification showWithStatus:tipStr styleName:JDStatusBarStyleSuccess];
    [JDStatusBarNotification showActivityIndicator:YES indicatorStyle:UIActivityIndicatorViewStyleWhite];
}

+ (void)ty_showStatusBarSuccessStr:(NSString *)successStr {
    if ([JDStatusBarNotification isVisible]) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [JDStatusBarNotification showActivityIndicator:NO indicatorStyle:UIActivityIndicatorViewStyleWhite];
            [JDStatusBarNotification showWithStatus:successStr dismissAfter:1.5 styleName:JDStatusBarStyleSuccess];
        });
    } else {
        [JDStatusBarNotification showActivityIndicator:NO indicatorStyle:UIActivityIndicatorViewStyleWhite];
        [JDStatusBarNotification showWithStatus:successStr dismissAfter:1.0 styleName:JDStatusBarStyleSuccess];
    }
}

+ (void)ty_showStatusBarErrorStr:(NSString *)errorStr {
    if ([JDStatusBarNotification isVisible]) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [JDStatusBarNotification showActivityIndicator:NO indicatorStyle:UIActivityIndicatorViewStyleWhite];
            [JDStatusBarNotification showWithStatus:errorStr dismissAfter:1.5 styleName:JDStatusBarStyleError];
        });
    } else {
        [JDStatusBarNotification showActivityIndicator:NO indicatorStyle:UIActivityIndicatorViewStyleWhite];
        [JDStatusBarNotification showWithStatus:errorStr dismissAfter:1.5 styleName:JDStatusBarStyleError];
    }
}

@end
