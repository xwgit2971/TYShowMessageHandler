//
//  TYShowMessageHandler.h
//  TYShowMessageHandler
//  显示HUD / 状态栏通知
//  Created by 夏伟 on 2016/11/3.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TYShowMessageHandler : NSObject

#pragma mark - MBProgressHUD
// 指示器 + 文字
+ (void)ty_showHUDQueryStr:(NSString *)titleStr;
+ (void)ty_showHUDQueryStr:(NSString *)titleStr toView:(UIView *)view;

// 隐藏Query HUD
+ (NSUInteger)ty_hideHUDQuery;
+ (NSUInteger)ty_hideHUDQueryForView:(UIView *)view;

// icon + 文字
+ (void)ty_show:(NSString *)text icon:(NSString *)icon;
+ (void)ty_show:(NSString *)text icon:(NSString *)icon view:(UIView *)view;

// Toast文字
+ (void)ty_showHudTipStr:(NSString *)tipStr;

#pragma mark - JDStatusBarNotification
// 指示器 + 文字 + Success Style
+ (void)ty_showStatusBarQueryStr:(NSString *)tipStr;
// 文字 + Success Style
+ (void)ty_showStatusBarSuccessStr:(NSString *)successStr;
// 文字 + Error Style
+ (void)ty_showStatusBarErrorStr:(NSString *)errorStr;

@end
