//
//  ViewController.m
//  TYShowMessageHandler
//
//  Created by 夏伟 on 2016/11/2.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "ViewController.h"
#import "TYShowMessageHandler.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (IBAction)hudBtnClicked:(id)sender {
//    [TYShowMessageHandler ty_showHUDQueryStr:@"加载中"];
      [TYShowMessageHandler ty_showHUDQueryStr:@"加载中" toView:self.view];
//    [TYShowMessageHandler ty_show:@"上传成功" icon:@"success" view:nil];
//    [TYShowMessageHandler ty_show:@"上传失败" icon:@"error" view:nil];
//    [TYShowMessageHandler ty_showHudTipStr:@"网络不给力哦..."];

//    [TYShowMessageHandler ty_showStatusBarQueryStr:@"上传中..."];
//    [TYShowMessageHandler ty_showStatusBarErrorStr:@"加载错误"];
//    [TYShowMessageHandler ty_showStatusBarSuccessStr:@"上传成功"];
}

- (IBAction)hideHud:(id)sender {
//    [TYShowMessageHandler ty_hideHUDQuery];
    [TYShowMessageHandler ty_hideHUDQueryForView:self.view];
}

@end
